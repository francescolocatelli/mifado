  rm(list=ls())
  gc()
  cat("\014")
  time0 <- Sys.time()


  ###---------------###
  # get directories   #
  ###---------------###
  
  home_current <- getwd()
  home_prj <- paste(Sys.getenv("HOME_STDA"),"/datiMoxoffContainer/",sep = "")
  home_results <- paste(home_prj,"script/results/",sep="")
  source(paste(home_prj,"script/","argsDT2_day.R",sep=""))
  source(paste(home_prj,"script/","utilitiesDT2_day.R",sep=""))

  home_customer <- paste(home_prj,select_customer,"/",sep="")
  home_customer_day <- paste(home_customer,select_day,sep="")
  home_act <- paste(home_customer_day,"/Activity/",sep="")
  home_click <- paste(home_customer_day,"/Click/",sep="")
  home_imp <- paste(home_customer_day,"/Impression/",sep="")
  home_match <- paste(home_customer_day,"/Matchtable/",sep="")
  
  
  print(paste("Customer: ",select_customer,sep=""))
  print(paste("Day analized: ",select_day," (dd-mm-YY)",sep=""))
  if (readDBMfields) {print("Import dati completi di campi DBM")} else print("Import dati senza campi DBM")
  
  ###--------------------------------###
  # posizione Customer #
  ###--------------------------------###
  
  posCustom <- which(names(customer) == select_customer)
  

  ###---------------###
  # import data       #
  ###---------------###
  
  ### Activity ###
  #====================
  
  # import activity
  time1 <- Sys.time()
  colClass <- customColClass(home_act,readDBMfields)
  act <- read.csv2(paste(home_act,list.files(path = home_act, pattern = "*.csv"),sep=""), 
                   sep=",",encoding = "UTF-8",colClasses = colClass)
  
  time2 <- Sys.time()
  
  ### Click ###
  #====================
  
  # file list of click
  clickListName <- list.files(path = home_click, pattern = "*.csv")
  
  setwd(home_click)
  colClass <- customColClass(home_click,readDBMfields)
  clickList <- lapply(paste(home_click,clickListName, sep=""), my.read.csv)
  click <- plyr::ldply(clickList,data.frame)
  
  # delete click list for saving space and re-set wd
  setwd(home_current)
  rm(clickList)
  time3 <- Sys.time()


  ### Impression ###
  #====================

  # file list of impression
  impListName <- list.files(path = home_imp, pattern = "*.csv")
   
  # import impression
  setwd(home_imp)
  colClass <- customColClass(home_imp,readDBMfields)
  impList <- lapply(paste(home_imp,impListName, sep="")[firstFileImp:lastFileImp], my.read.csv)
  imp <- plyr::ldply(impList,data.frame)

  # delete click list for saving space and re-set wd
  setwd(home_current)
  rm(impList)
  time4 <- Sys.time()

  
  ### Match Tables ###
  #====================
  
  # match tables list

  matchTable <- readMatchTableDay(home_match,posCustom)

  
 ###====================================================
 # DATA PROCESSING
 #======================================================
  
  ### Activity ###
  actClean <- preProcessingAct (act)
  
  ### Click ###   
  clickClean <- preProcessingClick(click)
  
  ### Impression ###
  time6 <- Sys.time()
  imp <- preProcessingImp(imp)
  time7 <- Sys.time()
  
  ### Cookie unici ###
  time8 <- Sys.time()
  cookieAct <- as.character(unique(actClean$UserID[which(actClean$UserID!=0)]))
  cookieClick <- as.character(unique(clickClean$UserID[which(clickClean$UserID!=0)]))
  cookieImp <- as.character(unique(imp$UserID[which(imp$UserID!=0)]))
  time9 <- Sys.time()
  
  ### All cookie dataset###
  # allCookieFields <- c("UserID","channel","EventType")
  # allCookie <- rbind(
  #       actClean[which(actClean$UserID!=0 & is.na(actClean$channel)==FALSE),allCookieFields],
  #       clickClean[which(clickClean$UserID!=0 & is.na(clickClean$channel)==FALSE),allCookieFields],
  #       imp[which(imp$UserID!=0 & is.na(imp$channel)==FALSE),allCookieFields]
  #       )
  # 
  # time10 <- Sys.time()
  # allCookie <- cbind(allCookie, predict(dummyVars(~ channel, data = allCookie),newdata = allCookie))
  # 
  # 
  # posChannelNames <- grep("channel.",names(allCookie))
  # allCookieUnique <- dplyr::select(allCookie,-channel,-EventType) %>%
  #                    group_by(UserID) %>%
  #                    summarise_each(funs(sum))
  # time11 <- Sys.time()

  ###====================================================
  # TABLE FOR DAY VALIDATION CHECK
  #====================================================== 
  
  tableList <- list()
  tableList$customer <- select_customer
  
  ### Activity ###
  #====================
  
  tableList$actC$Campaign_PlacementStrategy <- table(actClean$Campaign,actClean$PlacementStrategy,exclude = NULL)
  tableList$act$Campaign_channel <- table(actClean$Campaign,actClean$channel,exclude = NULL)
  tableList$act$PlacementStrategy_channel <- table(actClean$PlacementStrategy,actClean$channel,exclude = NULL)
  tableList$act$Site_channel <- table(actClean$SiteDCM,actClean$channel,exclude = NULL)
  tableList$act$Advertiser_channel <- table(actClean$Advertiser,actClean$channel,exclude = NULL)
  tableList$act$Site_cookie <- table(actClean$SiteDCM,actClean$cookie,exclude = NULL)
  tableList$act$cookie_channel <- table(actClean$cookie,actClean$channel,actClean$conversion)
  tableList$act$device_channel <- table(actClean$device,actClean$channel,actClean$conversion)
  

  ### Click ###
  #====================
  
  tableList$click$Campaign_PlacementStrategy <- table(clickClean$Campaign,clickClean$PlacementStrategy,exclude = NULL)
  tableList$click$Campaign_channel <- table(clickClean$Campaign,clickClean$channel,exclude = NULL)
  tableList$click$PlacementStrategy_channel <- table(clickClean$PlacementStrategy,clickClean$channel,exclude = NULL)
  tableList$click$Site_channel <- table(clickClean$SiteDCM,clickClean$channel,exclude = NULL)
  tableList$click$Advertiser_channel <- table(clickClean$Advertiser,clickClean$channel,exclude = NULL)
  tableList$click$Site_cookie <- table(clickClean$SiteDCM,clickClean$cookie,exclude = NULL)
  tableList$click$cookie_channel <- table(clickClean$cookie,clickClean$channel)
  tableList$click$device_channel <- table(clickClean$device,clickClean$channel)


  ### Impression ###
  #====================
  
  tableList$imp$Campaign_PlacementStrategy <- table(imp$Campaign,imp$PlacementStrategy,exclude = NULL)
  tableList$imp$Campaign_channel <- table(imp$Campaign,imp$channel,exclude = NULL)
  tableList$imp$PlacementStrategy_channel <- table(imp$PlacementStrategy,imp$channel,exclude = NULL)
  tableList$imp$Site_channel <- table(imp$SiteDCM,imp$channel,exclude = NULL)
  tableList$imp$Advertiser_channel <- table(imp$Advertiser,imp$channel,exclude = NULL)
  tableList$imp$Site_cookie <- table(imp$SiteDCM,imp$cookie,exclude = NULL)
  tableList$imp$cookie_channel <- table(imp$cookie,imp$channel)
  tableList$imp$device_channel <- table(imp$device,imp$channel)
  
  posImpValid <- which(imp$cookie=="valido")
    tableList$imp$Campaign_Valido_Eligibile_Measurable_Viewable <- cbind(
                        table(imp$Campaign[posImpValid],imp$ActiveViewEligibleImpressions[posImpValid],exclude = NULL),
                        table(imp$Campaign[posImpValid],imp$ActiveViewMeasurableImpressions[posImpValid],exclude = NULL),
                        table(imp$Campaign[posImpValid],imp$ActiveViewViewableImpressions[posImpValid],exclude = NULL)
                        )
  tableList$imp$channel_Valido_Eligibile_Measurable_Viewable <- cbind(
                         table(imp$channel[posImpValid],imp$ActiveViewEligibleImpressions[posImpValid],exclude = NULL),
                         table(imp$channel[posImpValid],imp$ActiveViewMeasurableImpressions[posImpValid],exclude = NULL),
                         table(imp$channel[posImpValid],imp$ActiveViewViewableImpressions[posImpValid],exclude = NULL)
                         )
  tableList$imp$Site_Valido_Eligibile_Measurable_Viewable <- cbind(
                         table(imp$SiteDCM[posImpValid],imp$ActiveViewEligibleImpressions[posImpValid],exclude = NULL),
                         table(imp$SiteDCM[posImpValid],imp$ActiveViewMeasurableImpressions[posImpValid],exclude = NULL),
                         table(imp$SiteDCM[posImpValid],imp$ActiveViewViewableImpressions[posImpValid],exclude = NULL)
                         )
  
  posImpOsc <- which(imp$cookie=="oscurato")
  tableList$imp$Campaign_Oscurato_Eligibile_Measurable_Viewable <- cbind(
                        table(imp$Campaign[posImpOsc],imp$ActiveViewEligibleImpressions[posImpOsc],exclude = NULL),
                        table(imp$Campaign[posImpOsc],imp$ActiveViewMeasurableImpressions[posImpOsc],exclude = NULL),
                        table(imp$Campaign[posImpOsc],imp$ActiveViewViewableImpressions[posImpOsc],exclude = NULL)
                        )
  tableList$imp$Campaign_Oscurato_Eligibile_Measurable_Viewable<- cbind(
                         table(imp$channel[posImpOsc],imp$ActiveViewEligibleImpressions[posImpOsc],exclude = NULL),
                         table(imp$channel[posImpOsc],imp$ActiveViewMeasurableImpressions[posImpOsc],exclude = NULL),
                         table(imp$channel[posImpOsc],imp$ActiveViewViewableImpressions[posImpOsc],exclude = NULL)
                         )
  tableList$imp$Campaign_Oscurato_Eligibile_Measurable_Viewable <- cbind(
                         table(imp$SiteDCM[posImpOsc],imp$ActiveViewEligibleImpressions[posImpOsc],exclude = NULL),
                         table(imp$SiteDCM[posImpOsc],imp$ActiveViewMeasurableImpressions[posImpOsc],exclude = NULL),
                         table(imp$SiteDCM[posImpOsc],imp$ActiveViewViewableImpressions[posImpOsc],exclude = NULL)
                         )
  
  ###---------------###
  # save tableList    #
  ###---------------###
  
  time12 <- Sys.time()
  save(tableList, file=paste(home_results,"tableList",select_day,select_customer,".RData",sep=""))
  time13 <- Sys.time()
  

# ==============================================================================================
  
  ###----------------------###
  # cookie che convertono   #
  ###---------------------###
  # cookieKconv <- unique(actClean$UserID[which(actClean$conv=="si conv")])
  # actConv <- actClean[which(actClean$UserID %in% cookieKconv),]
  # actConv <- actConv[which(actConv$UserID!=0),]
  # 
  # actCountConv <- group_by(actConv,UserID) %>% summarise(count=n())
  # # quanti cookie convertono prima dello step5?
  # length(which(actCountConv$count<5))
  # # quanti cookie convertono dopo aver effettuato più di 4 attività?
  # length(which(actCountConv$count>4))
  
  
  #### prove per pacchetto readr e calcolo parallelo
  
  # s<-grep("DBM",names(a))
  # b <- names(a)[s]
  # 
  # a<-read_csv(paste(home_act,list.files(path = home_act, pattern = "*.csv"),sep=""),
  #          fwf_positions(c(1,30),c(5,32))
  #          )
